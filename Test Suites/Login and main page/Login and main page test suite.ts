<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login and main page test suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>68b95b76-4a29-4cde-bf43-b8cd02d25051</testSuiteGuid>
   <testCaseLink>
      <guid>baf1d6c9-68e8-4b86-9a7a-f02f5378dedb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login with valid data</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>566a97c0-816e-45ae-a750-f41962a2df12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login with data binding</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8191caa3-58c2-4615-8843-893ada888ed8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/SauceDemoLogin</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>8191caa3-58c2-4615-8843-893ada888ed8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>d53546b5-3da0-451f-a556-c92a06fbe477</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8191caa3-58c2-4615-8843-893ada888ed8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>b44831b2-ab69-4da3-a01f-4d7d8e078acc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1dd5acdc-66c4-4e72-a228-dc7894b5a640</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Click product image_backpack</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2a5c68f0-a573-4bc9-936f-8573398f547d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Click product name_bike light</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1a50dd75-3f26-4fde-bad8-88f0c7c4ce0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Click product name_onesie</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1530e17c-33ec-4419-80d7-95544840b84e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Check button Back to Products</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
